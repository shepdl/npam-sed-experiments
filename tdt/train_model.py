import codecs
import sys

sys.path.append('.')

from tdt.importing import data
from gibbs.sampling import do_gibbs_sampling
from log import log

__author__ = 'Dave Shepard'


from types import *
import constants


def train_model():
    print "Loading data ..."
    experiment = data.load(constants.dictionary_filename, constants.data_filename)
    tweets = experiment['tweets']
    users = experiment['users']
    words = experiment['words']
    number_of_epochs = experiment['number_of_epochs']
    topics = [Topic(c) for c in xrange(constants.topic_count)]
    events = []
    print "Data loaded ..."

    print "Beginning training ..."
    count = 0
    for _ in range(constants.iteration_count):
        do_gibbs_sampling(tweets, words, topics, number_of_epochs, events)
        do_gradient_descent(events, users, topics)
        count += 1
        if count % 10 == 0:
            log("%s rounds complete" % (count,))

    log("Complete; writing files ...")
    output_file = codecs.open(constants.output_file, 'w', encoding='utf-8')
    # write out topics
    output_file.write('Topics:\n')
    for topic in topics:
        # get highest topic values
        top_words = topic.word_counts.items().sort(key=lambda p: p[1])[0:10]
        top_words_text = ['%s-%s ' % (words[word].text, count,) for word, count in top_words]
        output_file.write(
            '%s,%s\n' % (topic.id, top_words_text,)
        )
    # write out events:
    for event in events:
        top_words = event.word_counts.items().sort(key=1)[0:10]
        top_words_text = ['%s-%s' % (words[word].text, count,) for word, count in top_words]
        output_file.write(
            '%s,%s,%s\n' % (event.id, event.start_epoch, top_words_text,)
        )
    output_file.close()
    log("Complete!")


def do_gradient_descent(events, users, topics):
    """

    Given topic/event assignments, solve for ...
        eta[k]^0 -- event inner popularity (eta is the n-like character with the long right leg)
        eta[k] -- event-topic affinity vector

    :return:
    """

    def event_user_affinity(this_event, this_user):
        return this_event.inner_popularity + np.dot(this_event.topic_affinity, this_user.topic_distribution)
    for event in events:
        new_inner_popularity = -1 * constants.iota * event.inner_popularity + sum([
            constants.epsilon * user.event_distribution[event.id] * (1 - event_user_affinity(event, user))
            for user in users
        ])
        for topic in topics:
            new_topic_affinity = -1 * constants.iota * event.topic_affinity[topic.id] + sum([
                constants.epsilon * user.event_distribution[event.id] *
                    (1 - event_user_affinity(event, user)) * user.topic_distribution for user in users
            ])
            event.topic_affinity[topic.id] = new_topic_affinity
        event.inner_popularity = new_inner_popularity

if __name__ == "__main__":
    train_model()
