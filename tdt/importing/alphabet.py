__author__ = 'Dave Shepard'

import numpy as np
import string
import re

from twokenize import simpleTokenize

def clean_tweet(text):
    return ' '.join(re.sub(r'[^a-z]', '', tokenize(text.lower())))

def tokenize(text):
    return [re.sub(r'[^a-z]', '', t.lower()) for t in simpleTokenize(text) if t[0] not in string.punctuation and (
        len(t) > 4 and t[0:4] != 'http'
    )]


class Alphabet(dict):

    def __init__(self):
        self.word_to_index = {}
        self.index_to_words = None
        super(Alphabet, self).__init__(self)

    def parse_sentence(self, sentence):
        for token in tokenize(sentence):
            self.word_to_index.setdefault(token, 0)
            self.word_to_index[token] += 1
            # self.word_to_index[token] = None

    def complete(self):
        # self.index_to_words = np.zeros([len(self.word_to_index), ])
        # self.index_to_words = [''] * len(self.word_to_index)
        words_to_remove = [word for word, count in self.word_to_index.iteritems() if count <= 10]
        for word in words_to_remove:
            del self.word_to_index[word]
        self.index_to_words = [word for word, count in self.word_to_index.iteritems() if count > 10]
        for index, word in enumerate(self.index_to_words):
            # self.index_to_words[index] = word
            self.word_to_index[word] = index

    def words_to_alphabet(self, words):
        return [
            self.word_to_index[token] for token in tokenize(words) if token in self.word_to_index
        ]
