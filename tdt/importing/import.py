import codecs
import datetime
from tdt import constants

__author__ = 'Dave Shepard'

from pymongo import MongoClient
import pymongo


from alphabet import Alphabet


def alphabetize():
    conn = MongoClient()
    usertweets = conn.usertweets.tweets

    # Vocabulary file and dictionary
    alphabet = Alphabet()
    print "Generating alphabet ..."
    for tweet in usertweets.find({'lang' : 'en',}):
        alphabet.parse_sentence(tweet['text'])
    alphabet.complete()
    print "Tweet loading complete; writing file ..."
    dictionary_file = codecs.open(constants.dictionary_filename, 'w', encoding='utf-8')
    for item in enumerate(alphabet.index_to_words):
        dictionary_file.write('%s,%s\n' % item)
    dictionary_file.close()

    # Main data file
    print "Alphabet creation complete; writing main data file ..."
    data_file = codecs.open(constants.data_filename, 'w', encoding='utf-8')
    data_file.write("# Input data file\n")
    data_file.write("# Created on: %s \n" % datetime.datetime.now())

    # date stamps
    print "Finding start and end dates ..."
    for m in usertweets.find().sort('sent_at', pymongo.ASCENDING).limit(1):
        min_date = m['sent_at']
    data_file.write('Start date: %s\n' % (min_date,))
    for m in usertweets.find().sort('sent_at', pymongo.DESCENDING).limit(1):
        max_date = m['sent_at']
    data_file.write('End date: %s\n' % (max_date,))
    print "Writing users ..."

    import_users(data_file)

    # tweets
    print "Writing tweets ..."
    data_file.write('Tweets:\n')
    for tweet in usertweets.find({'lang' : 'en', }):
        tokens = [str(c) for c in alphabet.words_to_alphabet(tweet['text'])]
        if len(tokens) < 3:
            continue
        text = ' '.join(tokens)
        data_file.write("""%s,%s,%s,%s\n""" % (
                tweet['id'], 
                tweet['sent_at'], 
                tweet['user_name'], 
                text,
            )
        )
    data_file.close()
    print "Complete!"


def import_users(users_file):
    conn = MongoClient()
    usertweets = conn.usertweets.tweets
    # users_file = codecs.open('users.csv', 'w', encoding='utf-8')
    users_file.write('Users:\n')
    counter = 0
    for username in usertweets.distinct('user_name'): 
        users_file.write(str(counter))
        counter += 1
        users_file.write(',')
        users_file.write(username)
        users_file.write('\n')


if __name__ == "__main__":
    alphabetize()
