import codecs
import datetime

from ..types import Word, User, Tweet

__author__ = 'Dave Shepard'

"""
Methods for loading data
"""

class FileParseError(Exception):

    def __init__(self, message):
        super(FileParseError, self).__init__(message)


def date_to_epoch_converter(start_date):
    """
    Create a converter form a date time into an epoch offset

    :param start_date: datetime.datetime
    :return function converter function
    """
    assert isinstance(start_date, datetime.datetime)
    def _date_to_epoch(in_date, hours_in_epoch=1):
        """
        Convert a date time into an epoch offset
        :param in_date: datetime.datetime
        :return integer epoch offset
        """
        assert isinstance(in_date, datetime.datetime)
        delta = in_date - start_date
        return (delta.total_seconds / 3600) * hours_in_epoch
    return _date_to_epoch


def load(dictionary_file, data_filename):
    # load vocabulary
    # read headers
    data_file = codecs.open(data_filename, encoding='utf-8')
    # first line is header
    for line in data_file:
        if line[0] == "#":
            continue
        else:
            break

    data = read_header(data_file)
    vocabulary_data = load_dictionary(dictionary_file)
    data['vocabulary_size'] = vocabulary_data[0]
    data['vocabulary'] = vocabulary_data[1]

    data['users'] = read_users(data_file)
    epoch_from_date = date_to_epoch_converter(data['start_date'])
    data['number_of_epochs'] = epoch_from_date(data['end_date']) - epoch_from_date(data['start_date'])
    data['tweets'] = read_tweets(data_file, data['users'], epoch_from_date)
    return data


def load_dictionary(dictionary_file):
    in_file = codecs.open(dictionary_file, encoding='utf-8')
    vocab_size_counter = 0
    vocab = {}
    for line in in_file:
        index, word = line.split(',', 1)
        index = int(index)
        vocab[index] = Word(index, word)
        vocab_size_counter += 1
    return vocab_size_counter, vocab


def read_header(data_file):
    data = {}
    expected_date_format = '%Y-%m-%d %H:%M:%S'
    expected_start_date = data_file.readline()
    if "start date:" not in expected_start_date.lower():
        raise FileParseError("Expected start date; got: '\n\t%s' " % (expected_start_date,))
    data['start_date'] = datetime.datetime.strptime(
        expected_start_date.split(': ', 1)[1].strip(),
        expected_date_format
    )

    expected_end_date = data_file.readline()
    if "end date:" not in expected_end_date.lower():
        raise FileParseError("Expected end date; got: '\n\t%s' " % (expected_end_date,))
    data['end_date'] = datetime.datetime.strptime(
        expected_end_date.split(': ', 1)[1].strip(),
        expected_date_format
    )

    return data


def read_users(data_file):
    # skip blank lines
    for line in data_file:
        if not line or line =='\n':
            continue
        else:
            break

    expected_header = line
    if 'users:' not in expected_header.lower():
        raise FileParseError('Expected Users header; got: \n\t%s' % (expected_header,))
    users = {}
    is_username_line = True
    username = None
    for line in data_file:
        if line == '':
            break
        data = line.strip()
        if is_username_line:
            username = data
            users[username] = User(username, username)
        is_username_line = not is_username_line
    return users


def read_tweets(data_file, users, epoch_from_date):
    for line in data_file:
        if line == '':
            continue
        else:
            break

    expected_header = line
    if 'tweets:' not in expected_header.lower():
        raise FileParseError('Expected Tweets header; got: \n\t%s' % (expected_header,))

    tweets = {}
    for line in data_file:
        id, time_stamp, username, text = line.split(',', 4)
        epoch = epoch_from_date(time_stamp)
        user = users[username]
        tweets[id] = Tweet(id, text, user, epoch)

    return tweets
