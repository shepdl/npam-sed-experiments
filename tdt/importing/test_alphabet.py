from unittest import TestCase

from alphabet import Alphabet, tokenize, clean_tweet

__author__ = 'Dave Shepard'


class TestAlphabet(TestCase):

    def test_tokenize(self):
        raw_text = "Hello @username, I'm Bob!!! #greetings #LOL"
        tokenized = tokenize(raw_text)
        self.assertEqual(tokenized, ['hello', 'i\'m', 'bob', ])


    def test_clean_tweet(self):
        raw_tweet = "Hello @username, I'm Bob!!! #greetings #LOL"
        cleaned = clean_tweet(raw_tweet)
        self.assertEqual(cleaned, 'hello i\'m bob')

    input_data = """@dave: I'm gonna have some water and a glass of #milk!!!
        @dave: When do @you need me to put my #laundry together?
        @val: In about 20 minutes. #lol!!!!"""

    def test_transitivity_of_alphabetization(self):
        instance = Alphabet()

        for line in self.input_data.split('\n'):
            instance.parse_sentence(line)
        instance.complete()
        for line in self.input_data.split('\n'):
            for word in tokenize(line):
                self.assertEqual(
                    word,
                    instance.index_to_words[instance.word_to_index[word]]
                )


    def testTweets(self):
        instance = Alphabet()
        tokenized = []
        for line in self.input_data.split('\n'):
            instance.parse_sentence(line)
            tokenized.append(' '.join(tokenize(line)))
        instance.complete()
        test_comparison = []
        for line in self.input_data.split('\n'):
            if line:
                as_alphabet = instance.words_to_alphabet(tokenize(line))
                test_comparison.append(
                    ' '.join([
                        instance.index_to_words[word] for word in as_alphabet
                    ])
                )
        self.assertEqual(tokenized, test_comparison)