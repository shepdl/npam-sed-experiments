__author__ = 'Dave Shepard'

import numpy as np

import constants


class Tweet(object):
    EVENT = 0
    TOPIC = 1

    def __init__(self, _id, text, user, timestamp):
        self.id = _id
        self.text = text
        self.user = user
        self.epoch = timestamp
        self.event_or_topic = None
        self.assignment = None


class Event(object):

    def __init__(self, _id, start_epoch, number_of_epochs, number_of_topics, vocabulary_size):
        self.id = _id
        self.start_epoch = start_epoch
        self.end_epoch = None
        self.tweets = {} # keys: epoch -> list of tweets
        self.tweet_counts = np.zeros([number_of_epochs])
        self.topic_affinity = np.zeros([number_of_topics])
        self.word_distribution = np.zeros([vocabulary_size])
        self.inner_popularity = 1.0

    def end_at(self, epoch):
        self.end_epoch = epoch


class Topic(dict):

    def __init__(self, _id):
        self.id = _id
        self.word_counts = {}
        self.tweets = {}
        super(Topic, self).__init__()

    def inc(self, word):
        self.setdefault(word, 0)
        self[word] += 1

    def dec(self, word):
        # No error checking because this should never fail
        self[word] -= 1

    def words_in_topic(self):
        return sum([1 for value in self.word_counts.values() if value > 0])


class User(object):

    def __init__(self, _id, name):
        self.id = _id
        self.name = name
        self.tweets = []
        self.topic_distribution = np.zeros(constants.topic_count)
        self.event_distribution = np.zeros([100, ])

    def event_tweets(self):
        return [t for t in self.tweets if t.assignment == Tweet.EVENT]


class Word(object):

    def __init__(self, _id, text):
        self.id = _id
        self.text = text
        self.topic_count = np.zeros(constants.topic_count)
        self.event_count = np.zeros([100, ])


class ObjectDict(dict):

    def __init__(self, type):
        self.type = type
        super(ObjectDict, self).__init__()

    def add(self, **kwargs):
        item = self.type(**kwargs)
        self[item.id] = item
