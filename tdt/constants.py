__author__ = 'Dave Shepard'

alpha = 0.01
beta = 0.01
tau = 0.01
epsilon = 0.01
iota = 10
lambda_ = 0.01


topic_count = 40

iteration_count = 300


data_filename = 'input.data'
dictionary_filename = 'dictionary.dict'
output_file = 'output.csv'