from numbers import Number
import numpy as np

from tdt import constants
from log import log

__author__ = 'Dave Shepard'

from tdt.types import Tweet, Event, User, Word, Topic


def do_gibbs_sampling(tweets, words, topics, number_of_epochs, events):
    for tweet in tweets:

        # clean up old assignment
        user_tweet_topic_assignments_with_current = np.zeros([constants.topic_count])
        current_epoch = tweet.epoch
        event_counter = 0
        if tweet.assignment:
            user_tweet_topic_assignments_with_current = np.copy(tweet.user.topic_distribution)
            if tweet.event_or_topic == Tweet.EVENT:
                old_assignment = tweet.assignment
                del old_assignment.tweets[current_epoch][tweet.id]
                old_assignment.tweet_counts[current_epoch] -= 1
                old_assignment.word_distribution -= tweet.text
                for word in tweet.text:
                    words[word].event_count[old_assignment.id] -= 1
                tweets.user.event_distribution[old_assignment.id] -= 1
            else:
                del old_assignment[tweet.id]
                tweet.user.topic_distribution[old_assignment.id] -= 1
                for word in tweet.text:
                    old_assignment[word].dec()
                    words[word].topic_count[old_assignment.id] -= 1
            del tweet.assignment.tweets[tweet.id]

        new_event = events[-1]
        if not new_event or new_event.tweets[current_epoch]:
            new_event = Event(event_counter, current_epoch, number_of_epochs, constants.topic_count, len(words))
            event_counter += 1  # NOTE: do this here because all events are zero-indexed
            events.append(new_event)
        new_event.start_epoch = current_epoch

        event_tweet_probabilities = np.array([
            probability_of_event_tweet(tweet.text, event, tweet.user, len(words), current_epoch) for event in events
        ])
        event_tweet_probabilities = event_tweet_probabilities / np.sum(event_tweet_probabilities)
        topic_tweet_probabilities = np.array([
            # TODO: double-check this
            probability_of_topic_tweet(tweet, topic, tweet.user, len(words), user_tweet_topic_assignments_with_current)
            for topic in topics
        ])
        assignment_id = np.argmax(
            topic_tweet_probabilities.extend(event_tweet_probabilities)
        )
        assignment = None

        # Handle new assignment
        if assignment_id > constants.topic_count:  # Event
            assignment_id -= constants.topic_count
            assignment = events[assignment_id]
            assignment.tweets.setdefault(current_epoch, {})
            assignment.tweets[current_epoch][tweet.id] = tweet
            assignment.tweet_counts[current_epoch] += 1
            assignment.word_distribution += tweet.text
            for word in tweet.text:
                words[word].event_count[assignment_id] += 1
            tweet.user.event_distribution[assignment_id] += 1
        else:  # Topic
            assignment = topics[assignment_id]
            assignment[tweet.id] = tweet
            tweet.user.topic_distribution[assignment_id] += 1
            for word in tweet.text:
                assignment[word].inc()
                words[word].topic_count[assignment_id] += 1
    log("Gibbs sampling round complete")

def event_limitation(event_series, epoch_index):
    """
    Compute event limitation

    :param event_series: numpy.ndarray (types.Event.tweet_counts)
    :param epoch: int 
    :return float -- sum of event limitation
    """
    return sum([
        constants.lambda_ * np.abs(epoch_index - t) * count if np.abs(epoch_index - t) > 1 else 0
        for t, count in enumerate(event_series)
    ])


def event_limitations_of_all(events, epoch):
    return sum([
        event_limitation(event.id, epoch) for event in events
    ])


def rcrp(event_tweet_counts, epoch_index):
    """
    Do recurrent Chinese restaurant process

    :param event_tweet_counts: types.Event
    :param epoch_index: number
    :return: :raise LogicException:
    """
    assert event_tweet_counts[epoch_index - 1] >= 0
    if event_tweet_counts[epoch_index - 1] > 0:

        if event_tweet_counts[epoch_index] > 0:
            return (
                event_tweet_counts[epoch_index - 1] + event_tweet_counts[epoch_index]
            ) * (
                (
                    event_tweet_counts[epoch_index] + event_tweet_counts[epoch_index + 1]
                ) / event_tweet_counts[epoch_index]
            )
        elif event_tweet_counts[epoch_index] == 0:
            return event_tweet_counts[epoch_index - 1]
    elif (
        event_tweet_counts[epoch_index + 1] > 0
            and event_tweet_counts[epoch_index] == 0
    ):
        return event_tweet_counts[epoch_index + 1]
    else:  # event_id points to a new event
        return constants.alpha


def event_tweet_affinity_to_topics(
        event_inner_popularity, event_topic_tweet_counts,
        user_topic_distribution, distribution=np.random.normal
):
    """
    Compute the affinity of an event tweet to its topics

    :param event_inner_popularity: number (types.Event.inner_popularity)
    :param event_topic_tweet_counts: np.array (types.Event.topic_affinity)
    :param user_topic_distribution: np.array (types.User.topic_distribution)
    :return float (the event's affinity to all topics)
    """
    return distribution(
        event_inner_popularity +
        np.dot(event_topic_tweet_counts, user_topic_distribution)
        , constants.epsilon
    )


def word_distribution_product(tweet, word_count_vector, vocabulary_size):
    """
    Compute distribution of words

    :param tweet: dictionary of word counts
    :param word_count_vector: # TODO: is this redundant?
    :param vocabulary_size: Number
    :return float
    """
    word_counts = {}
    for word in tweet:
        word_counts.setdefault(word, 0)
        word_counts[word] += 1
    top_term = 1.0  # initialized to 1.0 because it accumulates the products of the top terms
    # TODO: refactor this using numpy
    for word, count in word_counts.iteritems():
        word_value = 0
        for i in xrange(count):
            word_value += word_count_vector[word] + i + constants.beta
        top_term *= word_value
    bottom_term = 1.0
    for i in xrange(len(words) - 1):
        bottom_term *= np.sum(word_count_vector) + i + vocabulary_size * constants.beta
    return top_term / bottom_term


def probability_of_event_tweet(tweet_text, event, user, word_count, current_epoch):
    """
    Compute probability that this tweet belongs to a certain event

    :param tweet_text: np.array
    :param event: types.Event
    :param user: types.User
    :param word_count: int
    :param current_epoch: int
    """
    nominator = (
        rcrp(event.tweet_counts, current_epoch) *
        event_tweet_affinity_to_topics(
            event.inner_popularity, event.topic_affinity,
            user.topic_distribution
        ) * np.exp(
            -1 * event_limitation(event.tweet_counts[current_epoch], current_epoch)
        )
    )
    return ((
        user.event_distribution[event.id] + constants.tau) /
        (np.sum(user.event_distribution) + 2 * constants.tau)
    ) * nominator * (
        word_distribution_product(tweet_text, event.word_distribution - tweet_text, word_count)
    )


def probability_of_topic_tweet(
        tweet, topic, user, total_words,
        user_tweet_topic_assignments_with_current,
        distribution=np.random.normal
):
    """
    Compute probability that tweet is a topic tweet

    :param tweet: types.Tweet
    :param topic: types.Topic
    :param user: types.User
    :param total_words: int
    :param user_tweet_topic_assignments_with_current: np.array
    :param distribution: function
    :return: float
    """
    probability = (
            (np.sum(user.topic_distribution) + constants.tau) /
            (np.sum(user.topic_distribution) + 2 * constants.tau)
        ) * (
            (user.topic_distribution[topic.id] + constants.tau) /
            (np.sum(user.topic_distribution) + constants.topic_count * constants.tau)
        ) * (
            word_distribution_product(tweet, topic.words_in_topic(), total_words)
        ) * (
            np.product([
                distribution(
                    tweet.assignment.inner_popularity + np.dot(
                        tweet.assignment.topic_affinity, user_tweet_topic_assignments_with_current
                    ), constants.epsilon
                ) /
                distribution(
                    tweet.assignment.inner_popularity + np.dot(
                        tweet.assignment.topic_affinity, user.topic_distribution
                    ), constants.epsilon
                )
                for tweet in user.event_tweets()
        ])
    )
    assert isinstance(probability, Number)
    return probability
