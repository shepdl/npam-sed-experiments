from unittest import TestCase
from gibbs.sampling import rcrp
from tdt import constants

__author__ = 'Dave Shepard'


class TestRCRP(TestCase):

    def do_test(self, input_tweet_counts, expected_value, epoch_index=1):
        self.assertEqual(
            rcrp(input_tweet_counts, epoch_index),
            expected_value
        )

    def test_event_still_strong(self):
        input_tweet_counts = [100, 100, 200]
        expected_value = (100 + 100) * ((100 + 200) / 100)
        self.do_test(input_tweet_counts, expected_value)

    def test_formerly_strong_event_returns_last_epoch(self):
        input_tweet_counts = [100, 0, 200]
        expected_value = 100
        self.do_test(input_tweet_counts, expected_value)

    def test_growing_event_returns_next_epoch(self):
        input_tweet_counts = [0, 0, 200]
        expected_value = 200
        self.do_test(input_tweet_counts, expected_value)

    def test_new_event_returns_alpha(self):
        input_tweet_counts = [0, 0, 0]
        expected_value = constants.alpha
        self.do_test(input_tweet_counts, expected_value)
