from unittest import TestCase

import numpy as np

from gibbs.sampling import event_tweet_affinity_to_topics
from tdt.types import Event, User

__author__ = 'Dave Shepard'


class TestEventTweetAffinityToTopics(TestCase):

    def test_math_is_correct(self):
        distribution_function = lambda x, y: x
        event = Event(1, 1, 100, 40, 100)
        event.inner_popularity = 1.0
        event.topic_affinity = [10,] * 40
        user = User(2, 'Demo')
        user.topic_distribution = np.array([10,] * 40)
        expected = event.inner_popularity + np.dot(
            event.topic_affinity, user.topic_distribution
        )
        self.assertEqual(
            event_tweet_affinity_to_topics(event.inner_popularity, event.topic_affinity, user.topic_distribution, distribution_function),
            expected
        )