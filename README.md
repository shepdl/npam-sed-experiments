SED Implementation
==================


A method for detecting nested categories of events (super-events and sub-events), based on NPAM (Nonparametric Bayes Pachinko Allocation).

Current status: data import works. The method itself has been implemented, but not tested yet.

See Git logs for project notes.

